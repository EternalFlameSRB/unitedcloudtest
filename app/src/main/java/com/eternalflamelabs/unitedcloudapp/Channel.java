package com.eternalflamelabs.unitedcloudapp;

import java.io.Serializable;

public class Channel implements Serializable {

    public final static String ID = "id";
    public final static String NAME = "name";
    public final static String NUMBER = "number";
    public final static String ICON = "icon";
    public final static String QUALITY = "quality";
    public final static String HD = "hd";
    public final static String FAVOURITE = "favourite";

    private long id;
    private String name;
    private String number;
    private String icon;
    private String quality;
    private boolean hd;
    private boolean favourite;
    private String url;

    public Channel() {
    }

    public Channel(long id, String name, String number, String icon, String quality, boolean hd, boolean favourite, String url) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.icon = icon;
        this.quality = quality;
        this.hd = hd;
        this.favourite = favourite;
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public boolean getHd() {
        return hd;
    }

    public void setHd(boolean hd) {
        this.hd = hd;
    }

    public boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}


