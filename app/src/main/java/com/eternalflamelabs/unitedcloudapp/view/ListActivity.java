package com.eternalflamelabs.unitedcloudapp.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TabHost;
import android.widget.Toast;

import com.eternalflamelabs.unitedcloudapp.Channel;
import com.eternalflamelabs.unitedcloudapp.adapters.ChannelAdapter;
import com.eternalflamelabs.unitedcloudapp.R;
import com.eternalflamelabs.unitedcloudapp.utils.HttpHandler;
import com.eternalflamelabs.unitedcloudapp.utils.RecyclerItemClickListener;
import com.eternalflamelabs.unitedcloudapp.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class ListActivity extends AppCompatActivity {

    private String TAG = ListActivity.class.getSimpleName();

    private ProgressDialog pDialog;

    private RecyclerView rv;
    private RecyclerView rv2;
    private RecyclerView rv3;

    private ArrayList<Channel> channelList;
    private ArrayList<Channel> favouriteList;
    private ArrayList<Channel> hdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists);

        channelList = new ArrayList<>();
        favouriteList = new ArrayList<>();
        hdList = new ArrayList<>();

        rv = (RecyclerView) findViewById(R.id.recycler_view);
        rv2 = (RecyclerView) findViewById(R.id.recycler_view2);
        rv3 = (RecyclerView) findViewById(R.id.recycler_view3);

        new ListActivity.GetChannel().execute();

        int tab_position = getIntent().getIntExtra("spec", 0);

        TabHost host = (TabHost)findViewById(R.id.tabHost);
        host.setup();

        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("All channels");
        spec.setContent(R.id.tab1);
        spec.setIndicator("All channels");
        host.addTab(spec);

        //Tab 2
        spec = host.newTabSpec("HD channels");
        spec.setContent(R.id.tab2);
        spec.setIndicator("HD channels");
        host.addTab(spec);

        //Tab 3
        spec = host.newTabSpec("Favourite");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Favourite");
        host.addTab(spec);

        host.setCurrentTab(tab_position);

        rv.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(),
                                                rv ,
                                                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Intent intent = new Intent(getApplicationContext(), StreamActivity.class);
                        intent.putExtra("id", channelList.get(position).getId());
                        intent.putExtra("name", channelList.get(position).getName());
                        intent.putExtra("icon", channelList.get(position).getIcon());
                        intent.putExtra("quality", channelList.get(position).getQuality());
                        intent.putExtra("hd", channelList.get(position).getHd());
                        intent.putExtra("favourite", channelList.get(position).getFavourite());
                        intent.putExtra("url", channelList.get(position).getUrl());
                        intent.putExtra("channelList", channelList);
                        intent.putExtra("position", String.valueOf(position));
                        Log.e(TAG, "Url je:" +channelList.get(position).getUrl());
                        startActivity(intent);
                    }
                })
        );

        rv2.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(),
                        rv2 ,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                                Intent intent = new Intent(getApplicationContext(), StreamActivity.class);
                                intent.putExtra("id", favouriteList.get(position).getId());
                                intent.putExtra("name", favouriteList.get(position).getName());
                                intent.putExtra("icon", favouriteList.get(position).getIcon());
                                intent.putExtra("quality", favouriteList.get(position).getQuality());
                                intent.putExtra("hd", favouriteList.get(position).getHd());
                                intent.putExtra("favourite", favouriteList.get(position).getFavourite());
                                intent.putExtra("url", favouriteList.get(position).getUrl());
                                intent.putExtra("channelList", favouriteList);
                                intent.putExtra("position", String.valueOf(position));
                                Log.e(TAG, "Url je:" +favouriteList.get(position).getUrl());
                                startActivity(intent);
                            }
                        })
        );

        rv3.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(),
                        rv3 ,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                                Intent intent = new Intent(getApplicationContext(), StreamActivity.class);
                                intent.putExtra("id", hdList.get(position).getId());
                                intent.putExtra("name", hdList.get(position).getName());
                                intent.putExtra("icon", hdList.get(position).getIcon());
                                intent.putExtra("quality", hdList.get(position).getQuality());
                                intent.putExtra("hd", hdList.get(position).getHd());
                                intent.putExtra("favourite", hdList.get(position).getFavourite());
                                intent.putExtra("url", hdList.get(position).getUrl());
                                intent.putExtra("channelList", hdList);
                                intent.putExtra("position", String.valueOf(position));
                                Log.e(TAG, "Url je:" +hdList.get(position).getUrl());
                                startActivity(intent);
                            }
                        })
        );

    }

    private class GetChannel extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ListActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(Utils.urlChannelJson);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    String logoUrlBase = jsonObj.getString("logo_url_base");
                    Log.e(TAG, "logo url base je: " + logoUrlBase);

                    String streamUrlBase = jsonObj.getString("stream_url_base");
                    Log.e(TAG, "logo url base je: " + streamUrlBase);

                    JSONArray channels = jsonObj.getJSONArray("channels_list");
                    channelList = new ArrayList<>();
                    favouriteList = new ArrayList<>();
                    hdList = new ArrayList<>();

                    for (int i = 0; i < channels.length(); i++) {
                        JSONObject c = channels.getJSONObject(i);

                        long id = c.getLong("id");
                        String name = c.getString("name");
                        String number = c.getString("num");
                        String icon = c.getString("pp");
                        String quality = c.getString("q");
                        boolean hd = c.getBoolean("hd");
                        boolean favorite = c.getBoolean("fav");

                        Channel channel = new Channel(id,
                                                      name,
                                                      number,
                                                      logoUrlBase+id+".png",
                                                      quality,
                                                      hd,
                                                      favorite,
                                                      streamUrlBase+"&channel="+icon+"&stream="+quality);

                        channelList.add(channel);
                        if (hd) {
                            hdList.add(channel);
                        }
                        if (favorite) {
                            favouriteList.add(channel);
                        }

                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ChannelAdapter adapter = new ChannelAdapter(getApplicationContext(), channelList);
            ChannelAdapter adapter2 = new ChannelAdapter(getApplicationContext(), favouriteList);
            ChannelAdapter adapter3 = new ChannelAdapter(getApplicationContext(), hdList);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getApplicationContext());
            RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getApplicationContext());

            rv.setLayoutManager(mLayoutManager);
            rv.setItemAnimator(new DefaultItemAnimator());
            rv.setAdapter(adapter);

            rv2.setLayoutManager(mLayoutManager2);
            rv2.setItemAnimator(new DefaultItemAnimator());
            rv2.setAdapter(adapter2);

            rv3.setLayoutManager(mLayoutManager3);
            rv3.setItemAnimator(new DefaultItemAnimator());
            rv3.setAdapter(adapter3);

            Collections.sort(channelList, new Utils.CustomComparator());
            Collections.sort(favouriteList, new Utils.CustomComparator());
            Collections.sort(hdList, new Utils.CustomComparator());

            adapter.notifyDataSetChanged();
            adapter2.notifyDataSetChanged();
            adapter3.notifyDataSetChanged();
        }

    }

}
