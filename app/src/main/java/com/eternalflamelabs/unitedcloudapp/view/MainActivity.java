package com.eternalflamelabs.unitedcloudapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import android.widget.AdapterView;
import android.widget.GridView;

import com.eternalflamelabs.unitedcloudapp.adapters.MainActivityAdapter;
import com.eternalflamelabs.unitedcloudapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainactivity);

        GridView gridView = (GridView)findViewById(R.id.grid_view);
        gridView.setAdapter(new MainActivityAdapter(this));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        openAllChannels(view);
                        return;
                    case 1:
                        openFavouriteChannels(view);
                        return;
                    case 2:
                        openHdChannels(view);
                        return;
                    case 3:
                        openAboutUs(view);
                        return;
                    default:
                }
            }
        });
    }

    public void openAllChannels(View view) {

        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra("spec", 0);
        startActivity(intent);
    }

    public void openFavouriteChannels(View view) {

        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra("spec", 2);
        startActivity(intent);
    }

    public void openHdChannels(View view) {

        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra("spec", 1);
        startActivity(intent);
    }

    public void openAboutUs(View view) {

        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
}