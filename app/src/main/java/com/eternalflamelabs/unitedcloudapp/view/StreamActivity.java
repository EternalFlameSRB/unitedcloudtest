package com.eternalflamelabs.unitedcloudapp.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.eternalflamelabs.unitedcloudapp.Channel;
import com.eternalflamelabs.unitedcloudapp.R;
import com.eternalflamelabs.unitedcloudapp.adapters.StreamAdapter;
import com.eternalflamelabs.unitedcloudapp.utils.HttpHandler;
import com.eternalflamelabs.unitedcloudapp.utils.RecyclerItemClickListener;
import com.eternalflamelabs.unitedcloudapp.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class StreamActivity extends Activity {

    private String TAG = StreamActivity.class.getSimpleName();

    private ArrayList<Channel> streamList;
    private RecyclerView rv;
    private ProgressDialog pDialog;

    private VideoView mVideoView;
    private ImageView favouriteView;
    ImageView hdView;
    ImageView logo;
    private TextView title;
    private Button buttonList;
    private String url;
    private String name;
    private Uri video;
    private int position;
    boolean hd;
    boolean favourite;
    boolean visible = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream);

        rv = (RecyclerView) findViewById(R.id.recycler_view);
        favouriteView = (ImageView) findViewById(R.id.favourite);
        hdView = (ImageView) findViewById(R.id.quality);
        logo = (ImageView) findViewById(R.id.logo);
        title = (TextView) findViewById(R.id.title);
        buttonList = (Button) findViewById(R.id.channels);
        buttonList.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!visible) {
                    rv.setVisibility(View.VISIBLE);
                    visible = !visible;
                    buttonList.setText(R.string.hide_channels);
                    Toast.makeText(getApplicationContext(), "Channels list visible", Toast.LENGTH_SHORT).show();
                } else {
                    rv.setVisibility(View.INVISIBLE);
                    visible = false;
                    buttonList.setText(R.string.show_channels);
                    Toast.makeText(getApplicationContext(), "Channels list hidden", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageView back_button = (ImageView) findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Intent i = getIntent();
        position = Integer.valueOf(i.getStringExtra("position"));
        url = i.getStringExtra("url");
        hd = i.getBooleanExtra("hd", hd);
        if (hd){
            hdView.setImageResource(R.drawable.hd_on);
            hdView.setVisibility(View.VISIBLE);
        }
        favourite = i.getBooleanExtra("favourite", favourite);
        if (favourite){
            favouriteView.setImageResource(R.drawable.fav_on);
            favouriteView.setVisibility(View.VISIBLE);
        }
        name = i.getStringExtra("name");
        title.setText(name);

        video = Uri.parse(i.getStringExtra("url"));

        Log.e(TAG, "Url u stream activity je:"+url);
        Log.e(TAG, "URI u stream activity je:"+video);

        mVideoView = (VideoView) findViewById(R.id.surface_view);
        mVideoView.setVideoURI(video);
        mVideoView.setMediaController(new MediaController(StreamActivity.this));
        mVideoView.requestFocus();
        mVideoView.postInvalidateDelayed(100);

        new Thread(new Runnable() {
            public void run() {
                mVideoView.start();

            }
        }).start();

        new StreamActivity.GetChannel().execute();

        mVideoView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int action = MotionEventCompat.getActionMasked(event);

                switch(action) {
                    case (MotionEvent.ACTION_DOWN) :
                        Log.d(TAG,"Action was DOWN");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rv.findViewHolderForAdapterPosition(position).itemView.performClick();
                            }
                        },position--);
                        return true;
                    case (MotionEvent.ACTION_UP) :
                        Log.d(TAG,"Action was UP");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rv.findViewHolderForAdapterPosition(position).itemView.performClick();
                            }
                        },position++);
                        return true;
                    default :
                        return StreamActivity.super.onTouchEvent(event);
                }
            }
        });

        rv.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(),
                        rv ,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                    Intent intent = new Intent(getApplicationContext(), StreamActivity.class);
                                    intent.putExtra("id", streamList.get(position).getId());
                                    intent.putExtra("name", streamList.get(position).getName());
                                    intent.putExtra("icon", streamList.get(position).getIcon());
                                    intent.putExtra("quality", streamList.get(position).getQuality());
                                    intent.putExtra("hd", streamList.get(position).getHd());
                                    intent.putExtra("favourite", streamList.get(position).getFavourite());
                                    intent.putExtra("url", streamList.get(position).getUrl());
                                    intent.putExtra("channelList", streamList);
                                    intent.putExtra("position", String.valueOf(position));
                                Log.e(TAG, "Url je:" +streamList.get(position).getUrl());
                                    startActivity(intent);
                                }
                        })
        );

    }

    public class GetChannel extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(StreamActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(Utils.urlChannelJson);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    String logoUrlBase = jsonObj.getString("logo_url_base");
                    Log.e(TAG, "logo url base je: " + logoUrlBase);

                    String streamUrlBase = jsonObj.getString("stream_url_base");
                    Log.e(TAG, "logo url base je: " + streamUrlBase);

                    JSONArray channels = jsonObj.getJSONArray("channels_list");
                    streamList = new ArrayList<>();

                    for (int i = 0; i < channels.length(); i++) {
                        JSONObject c = channels.getJSONObject(i);

                        long id = c.getLong("id");
                        String name = c.getString("name");
                        String number = c.getString("num");
                        String icon = c.getString("pp");
                        String quality = c.getString("q");
                        boolean hd = c.getBoolean("hd");
                        boolean favorite = c.getBoolean("fav");

                        Channel channel = new Channel(id, name, number, logoUrlBase+id+".png", quality, hd, favorite, streamUrlBase + "&channel=" + icon + "&stream=" + quality);

                        streamList.add(channel);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            StreamAdapter adapter = new StreamAdapter(getApplicationContext(), streamList);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

            rv.setLayoutManager(mLayoutManager);
            rv.setItemAnimator(new DefaultItemAnimator());
            rv.setAdapter(adapter);

            Collections.sort(streamList, new Utils.CustomComparator());

            adapter.notifyDataSetChanged();

        }

    }



}