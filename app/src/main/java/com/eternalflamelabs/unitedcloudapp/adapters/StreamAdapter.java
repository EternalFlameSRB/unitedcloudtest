package com.eternalflamelabs.unitedcloudapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eternalflamelabs.unitedcloudapp.Channel;
import com.eternalflamelabs.unitedcloudapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by EternalFlame on 08-Nov-16.
 *
 */

public class StreamAdapter extends RecyclerView.Adapter<StreamAdapter.MyViewHolder> {

    private Context context;
    private List<Channel> streamList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView list_image, favourite;
        private TextView title, stream_quality_number, channel_number;

        MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            stream_quality_number = (TextView) view.findViewById(R.id.stream_quality_number);
            channel_number = (TextView) view.findViewById(R.id.channel_number);
            list_image = (ImageView) view.findViewById(R.id.list_image);
            favourite = (ImageView) view.findViewById(R.id.favourite);
        }
    }


    public StreamAdapter(Context context, List<Channel> streamListList) {
        this.context = context;
        this.streamList = streamListList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_small, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Channel channel = streamList.get(position);
        holder.title.setText(channel.getName());
        holder.stream_quality_number.setText(channel.getQuality());
        holder.channel_number.setText(channel.getNumber());
        holder.list_image.setImageResource(R.drawable.television);
        Picasso.with(context).load(channel.getIcon()).into(holder.list_image);
        if (channel.getFavourite()) {
            holder.favourite.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return streamList.size();
    }

}
