package com.eternalflamelabs.unitedcloudapp.utils;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;

import com.eternalflamelabs.unitedcloudapp.Channel;

import java.util.Comparator;


/**
 * Created by EternalFlame on 08-Nov-16.
 *
 */

public class Utils {

    public static final String urlChannelJson = "http://93.103.22.144/konkurs/list.json";

    public static void hideKeyboard(Activity activity,
                                    IBinder windowToken) {
        InputMethodManager mgr =
                (InputMethodManager) activity.getSystemService
                        (Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken,
                0);
    }

    /**
     * Ensure this class is only used as a utility.
     */
    private Utils() {
        throw new AssertionError();
    }

    public final static class CustomComparator implements Comparator<Channel> {
        @Override
        public int compare(Channel o1, Channel o2) {
            return Integer.valueOf(o1.getNumber()).compareTo(Integer.valueOf(o2.getNumber()));
        }
    }
}
